/* jshint evil:true */
$(function() {
  var selectContents = function(elem) {
    if (!elem) return;

    var range;

    if (document.selection) {
      range = document.body.createTextRange();
      range.moveToElementText(elem);
      range.select();
    } else if (window.getSelection) {
      range = document.createRange();
      range.selectNode(elem);
      window.getSelection().addRange(range);
    }
  };

  var run = function() {
    var json = $('#jsonInput').val();
    var select = false;
    if (json) {
      try {
        json = eval('(' + json + ')');
        json = JSON.stringify(json, null, '  ');
        select = true;
      } catch (ex) {
        json = ex.toString();
      }
    }

    $('#jsonOutputContainer').html('<pre>' + json + '</pre>');
    if (select) {
      selectOutput();
    }
  };
  var selectInput = function() {
    var elem = $('#jsonInput')[0];
    if (!elem) return;

    elem.focus();
    elem.select();
    //selectContents(elem);
  };
  var clearInput = function() {
    $('#jsonInput').val('');
    selectInput();
  };

  var selectOutput = function() {
    selectContents($('#jsonOutputContainer pre')[0]);
  };
  var clearOutput = function() {
    $('#jsonOutputContainer').html('');
  };

  //events
  // input
  $('#runBtn').click(run);
  $('#selectInputBtn').click(selectInput);
  $('#clearInputBtn').click(clearInput);
  // output
  $('#selectOutputBtn').click(selectOutput);
  $('#clearOutputBtn').click(clearOutput);
});
